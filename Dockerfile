FROM node:20.11.1-bullseye-slim

WORKDIR /app

COPY package.json package-lock.json /app/

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm","start"]
